
* Emacs configuration---J. Steen Hoyer

I'm experimenting again with keeping my emacs config public.

** Clone emacs-lisp packages needed

#+BEGIN_SRC sh
git clone --depth 1 https://github.com/magit/magit.git
git clone --depth 1 https://github.com/magit/git-modes.git

git clone --depth 1 https://github.com/emacs-ess/ESS.git
git clone --depth 1 https://github.com/vitoshka/polymode.git

git clone --depth 1 git://jblevins.org/git/markdown-mode.git


# git clone --depth 1 https://github.com/wash/pubmode.git
git clone --depth 1 git@github.com:jshoyer/pubmode.git
#+END_SRC

** Update lisp

#+BEGIN_SRC sh

cd ESS; git pull
cd git-modes; git pull
cd magit; git pull
cd markdown-mode; git pull
cd polymode; git pull
cd pubmode; git pull

#+END_SRC
