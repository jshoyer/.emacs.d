
;; ** General settings
;; *** the toolbar and the startup message get old pretty fast; backup is annoying
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(setq
 inhibit-startup-message t
 initial-scratch-message "
 "
 backup-inhibited t
 show-paren-mode t
 split-width-threshold 131  ; I prefer to work with a single window split vertically
 split-height-threshold 7000000
 visible-bell t
 )

;; *** IBuffer, hl-line, recentf
(recentf-mode)
;(global-hl-line-mode t)

(require 'ibuffer)
(require 'ibuf-ext)
(defalias 'list-buffers 'ibuffer)
(setq ibuffer-never-show-predicates
      (list "^\\*scratch" "^\\*ESS" "^\\*Calendar"
	    "^\\*Org Agenda"  "^\\*tramp" ;; "^\\init.el"
	    "^\\custom.el"   ;; Why does this one not get hidden?
	    "^\\org-config.org"
	    "^\\*Messages" "^\\*Completions"))

;; *** Auto refresh buffers
;; http://whattheemacsd.com/sane-defaults.el-01.html
(global-auto-revert-mode 1)
(setq global-auto-revert-non-file-buffers t)
(setq auto-revert-verbose nil)

;; UTF-8 please (also from Magnar S.)
(setq locale-coding-system 'utf-8) ; pretty
(set-terminal-coding-system 'utf-8) ; pretty
(set-keyboard-coding-system 'utf-8) ; pretty
(set-selection-coding-system 'utf-8) ; please
(prefer-coding-system 'utf-8) ; with sugar on top

;; *** Ido stuff that I don't entirely understand
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)
(ido-mode 1)
(setq ido-use-filename-at-point 'guess)
; (ffap-bindings)
;; seems to override

(setq ido-file-extensions-order '(".org" ".txt" ".py" ".emacs" ".xml" ".el" ".ini" ".cfg" ".cnf"))
(setq ido-default-buffer-method "selected-window")

;; *** Live freely/dangerously/confusingly

(put 'narrow-to-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)


;; *** Clumsy dired hook
(add-hook 'dired-mode-hook 'toggle-truncate-lines)

;; ** ESS
;; ** Load ESS, indent, nuke whitespace, make up arrow work

(load "~/.emacs.d/ESS/lisp/ess-site")
; (setq ess-help-own-frame 'one)
; (setq ess-help-frame-alist '((height . 55) (width . 80) (unsplittable . t)))

;; http://cran.r-project.org/doc/manuals/R-ints.html#R-coding-standards
(add-hook 'ess-mode-hook
	  (lambda ()
	    (ess-set-style 'C++ 'quiet)
	    (add-hook 'local-write-file-hooks
		      (lambda ()
			(ess-nuke-trailing-whitespace)))))
(setq ess-nuke-trailing-whitespace-p t)
(add-hook 'perl-mode-hook
	  (lambda () (setq perl-indent-level 4)))

(setq ess-history-file nil)		; info:ess#Saving
(setq ess-tab-complete-in-script t)

(eval-after-load "comint"		; info:ess#Command
  '(progn
     (define-key comint-mode-map [up]
       'comint-previous-matching-input-from-input)
     (define-key comint-mode-map [down]
       'comint-next-matching-input-from-input)

     (setq comint-scroll-to-bottom-on-output 'others
	   comint-scroll-show-maximum-output t
	   comint-scroll-to-bottom-on-input 'this
     ))
)

;; *** Org-struct, reftex
(add-hook 'ess-mode-hook 'turn-on-orgstruct++)
(setq orgstruct-heading-prefix-regexp "#' ")
;(add-hook 'emacs-lisp-mode-hook 'turn-on-orgstruct++)

(add-hook 'latex-mode-hook 'turn-on-reftex)   ; with Emacs latex mode

;; *** Polymode

(add-to-list 'load-path "~/.emacs.d/markdown-mode/")

(setq load-path
      (append '("~/.emacs.d/polymode/"
		"~/.emacs.d/polymode/modes")
              load-path))

(require 'poly-R)
(require 'poly-markdown)
(require 'poly-org)

(add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))


;; ** Gap here---don't rename or delete files!
;; ** No pubmode for now

;; ** VCS (Magit)
(add-to-list 'load-path "~/.emacs.d/git-modes")
(add-to-list 'load-path "~/.emacs.d/magit")
(eval-after-load 'info
  '(progn (info-initialize)
          (add-to-list 'Info-directory-list "~/.emacs.d/magit/")))
					; remember to make docs
(require 'magit)
					; try this, from john kitchin
;; Disable all version control. makes startup and opening files much faster
(setq vc-handled-backends nil)


;; ** Keybindings
(global-set-key "\C-cs" 'magit-status)

(setq-default ess-dialect "R")
(global-set-key "\C-cr" 'ess-switch-to-inferior-or-script-buffer)

(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-co" 'org-open-at-point-global)
(global-set-key "\C-cg" 'org-clock-goto)

(global-set-key "\C-cc" 'org-capture)
(setq org-context-in-file-links nil)

;; *** Org-babel
;; [[info:org#Working with source code]]
(org-babel-do-load-languages
 'org-babel-load-languages
 '((emacs-lisp . t)
   (R . t)
   (sh . t)))

(setq org-confirm-babel-evaluate nil)

;; *** R assignment operator as in RStudio
(defun arrow () "Insert <-"
  (interactive) (insert " <- "))
(global-set-key "\M--" 'arrow)  ;; overwrites a binding, but several are available.

(ess-toggle-underscore nil)

(defun longarrow () "Insert -->"
  (interactive) (insert " --> "))
(global-set-key "\M-_" 'longarrow)

;; *** Symbols
(fset 'mu "\C-x8m")
(global-set-key (kbd "M-m") 'mu)
(fset 'deg "\C-x8o")
(global-set-key (kbd "M-0") 'deg)
;(fset 'delta "\C-x8")   ; #X03B4
;(global-set-key (kbd "M-j") 'delta)

;; replaced above
(global-set-key (kbd "\C-cb") 'back-to-indentation)

;; *** Clumsy instant time-stamp insertion
(defun yyyymmdd () "insert YYYY-MM-DD date/timestamp"
  (interactive)
  (insert (eval
   (format-time-string "%Y-%m-%d"))))

(defun yyyymmddbracket () "insert [YYYY-MM-DD] date/timestamp"
  (interactive)
  (insert (eval
   (format-time-string "[%Y-%m-%d]"))))

(defun yyyymmddhhmm () "insert YYYY-MM-DD date/timestamp"
  (interactive)
  (insert (eval
   (format-time-string "%Y-%m-%d %R"))))

(defun yyyymmddhhmmbracket () "insert [YYYY-MM-DD HH:MM] date/timestamp"
  (interactive)
  (insert (eval
   (format-time-string "[%Y-%m-%d %R]"))))

(global-set-key (kbd "C-c d") 'yyyymmddbracket)
(global-set-key (kbd "s-d") 'yyyymmdd)
(global-set-key (kbd "C-c t") 'yyyymmddhhmmbracket)
(global-set-key (kbd "s-D") 'yyyymmddhhmm)


;; *** Awkward display stuff

(defun left () "Move frame to left of ViewSonic monitor"
       (interactive) (set-frame-position last-event-frame 0 0))
(global-set-key (kbd "C-c 1") 'left)

(defun center () "Move frame to center of ViewSonic monitor"
       (interactive) (set-frame-position last-event-frame 600 0))
(global-set-key (kbd "C-c 2") 'center)

(defun right () "Move frame to right of ViewSonic monitor"
       (interactive) (set-frame-position last-event-frame 1200 0))
(global-set-key (kbd "C-c 3") 'right)

(defun wider () "Make current frame wider, split it horizontally"
  (interactive)
  (set-frame-width (selected-frame) 177)
  (split-window-horizontally))

(global-set-key "\C-cw" 'wider)

(defun shorty () "Make current frame short, remove splits"
  ; todo: set font to non-fixed width, increase size
  (interactive)
  (set-frame-height (selected-frame) 34)
  (delete-other-windows)
)

(global-set-key "\C-cy" 'shorty)

(global-set-key "\C-cz" 'minimize-window)

